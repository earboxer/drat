# DRAT

## Dockerized-Repository Activation-Tool

Goals of drat are to simplify launching a dockerized application.

Use `drat help` to see all commands that should work.
If you have a drat-enabled application, the following commands should work.

This project is inspired by a similar project by [willg101](https://github.com/willg101).

# Dependencies

`docker` and `docker-compose` will be required

Nginx or Apache or Pagekite will be used as proxies.

Some connection to Letsencrypt will generate certificates.

# Extensibility

This project should be easily extensible.
Extensions should be able to read additional keys, uncomment `docker-compose.yml` lines,
and inject things before all hooks.

# Related projects

"Don't re-invent the wheel"

* https://github.com/jwilder/nginx-proxy (MIT)
* https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion (MIT)
